import { Component, OnInit } from '@angular/core';
import { info, actions } from 'src/app/utils/mock.data';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  reductions = [];
  actionsOnConsumption = actions;
  private readonly people = info.emplacements * info.nb_people;
  readonly currentMonth = 8;
  readonly actualConsumption = 0;
  nights = 3350;
  consumption = 95;
  totalConsumption = 0;
  limitConsumption = 0;
  annualConsumption = 100;
  readonly date = new Date();
  constructor() {
    let currentConsumption = 0;
    for (let i = 0; i < this.currentMonth; ++i) {
      const nbPeople = this.people * info.reservations[i];
      const conso = nbPeople * info.consommation[i];
      this.actualConsumption += conso;
      currentConsumption += conso;
      this.totalConsumption += conso;
    }

    for (let i = this.currentMonth; i < 12; i++) {
      const nbPeople = this.people * info.reservations[i];
      const conso = nbPeople * info.consommation[i];
      this.totalConsumption += conso;
    }

    this.consumption = currentConsumption * 100 / this.totalConsumption;
    this.limitConsumption = info.authorized * 100 / this.totalConsumption;
  }

  ngOnInit() {

  }

  sliderValueChanged($event) {
    this.nights = 10 * $event.value;
    this.consumption = $event.value * 95 / 3350;
  }

  checked( $event, value) {
    if ($event.checked) {
      this.reductions.push(value);
    } else {
      this.reductions = this.reductions.filter( v => v !== value);
    }

    let remaining = this.totalConsumption - this.actualConsumption;
    this.reductions
      .map( red => remaining * red)
      .map( red => remaining = remaining - red);
    const newAnnualConsumption = this.actualConsumption + remaining;
    this.annualConsumption = newAnnualConsumption * 100 / this.totalConsumption;
  }

  getBackgroundColor() {
    return this.annualConsumption > this.limitConsumption ? '#ac3d3a' : '#7ca036';
  }
}
