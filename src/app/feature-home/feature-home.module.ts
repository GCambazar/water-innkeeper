import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './components/home/home.component';
import { AgmCoreModule } from '@agm/core';
import { FeatureHomeRoutingModule } from './feature-home-routing.module';
import { BackendService } from '../utils';
import { MaterialModule } from '../utils/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { GestureConfig } from '@angular/material';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    FeatureHomeRoutingModule,
    AgmCoreModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule
  ],
  providers: [BackendService,
    { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig },

  ]
})
export class FeatureHomeModule { }
