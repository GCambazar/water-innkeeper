import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  {
    path: 'home',
    loadChildren: './feature-home/feature-home.module#FeatureHomeModule'
  },
  {
    path: 'profile',
    loadChildren: './feature-profile/feature-profile.module#FeatureProfileModule'
  },
  {
    path: 'status',
    loadChildren: './feature-status/feature-status.module#FeatureStatusModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
