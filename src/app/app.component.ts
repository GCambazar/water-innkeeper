import { Component, OnInit } from '@angular/core';
import { BackendService } from './utils';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'water-tech-hackathon';

  constructor( private readonly service: BackendService,
    private readonly router: Router) {}
  ngOnInit() {
  }

  getClass(url: string) {
    return ((this.router.url.indexOf(url) !== -1) ? 'selected' : '');
  }
}
