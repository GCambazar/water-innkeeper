import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeatureStatusRoutingModule } from './feature-status-routing.module';
import { StatusComponent } from './components/status/status.component';

@NgModule({
  declarations: [StatusComponent],
  imports: [
    CommonModule,
    FeatureStatusRoutingModule
  ]
})
export class FeatureStatusModule { }
