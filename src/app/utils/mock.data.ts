export const actions = [ {
    label: 'Réduire arrosage',
    ratio: 0.2
    },
    {
        label: 'Changer filtres',
        ratio: 0.12
    },
    {
        label: 'Couper eau',
        ratio: 0.45
    }
];

export const info = {
    emplacements: 2,
    nb_people: 4,
    reservations: [
        0,
        0,
        12,
        25,
        45,
        80,
        100,
        98,
        71,
        63,
        10,
        0
    ],
    consommation: [
        150,
        150,
        150,
        200,
        200,
        250,
        250,
        250,
        250,
        200,
        150,
        150
    ],
    authorized: 800000
};
