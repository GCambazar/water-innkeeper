
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpParams,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../+state/app-state/app.interfaces';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  constructor(private readonly store: Store<AppState> ) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).pipe(
        tap( () => {}, (err: any) => {
        if (err instanceof HttpErrorResponse) {
                if (request.params) {
                    const params: HttpParams = request.params;
                }
            }
        }
    )
    );
}
}
