// this interface is used for the entity state of our store
export interface City {
    name: string;
}


export interface Equipements {
    piscine: number;
    douche: number;
    espaceVertm2: number;
    machineALaver: number;
    wc: number;
    fuite: number;
}
