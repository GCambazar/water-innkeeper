import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Store } from '@ngrx/store';
import { Showers, Toilets } from '../+state/app-state/app.actions';

@Injectable()
export class BackendService {

  // private readonly showers = 'showers';
  // private readonly toilets = 'toilets';

  // private showersDb: AngularFirestoreCollection<any>;
  // private toiletsDb: AngularFirestoreCollection<any>;
  constructor(
    private db: AngularFirestore,
    private storage: AngularFireStorage,
    private store: Store<any>
  ) {
    // this.showersDb = this.db.collection<any>(this.showers);
    // this.toiletsDb = this.db.collection<any>(this.toilets);
  }

  // getAllShowers() {
  //   this.showersDb.valueChanges().subscribe( itemsInDb => this.store.dispatch(new Showers({ showers: itemsInDb})));
  // }

  // getAllToilets() {
  //   this.toiletsDb.valueChanges().subscribe( itemsInDb => this.store.dispatch(new Toilets({toilets: itemsInDb})));
  // }

  // addShower() {
  //   this.showersDb.add( {
  //     date: new Date().toString()
  //   });
  // }

  // addToilet() {
  //   this.toiletsDb.add( {
  //     date: new Date().toString()
  //   });
  // }

  // saveOrUpdateRoute(trajectoryName: string, route: Point[] ) {
  //   const me = this;
  //   this.trajectoriesDB.doc(trajectoryName)
  //   .set(
  //     {route}, {merge: true}
  //   )
  //   .catch(err => console.log(err));
  // }
  // stop() {}
}
