
export interface App {
    showers: [];
    toilets: [];
  }

  export interface AppState {
    readonly application: App;
  }

  export const AppInitialState: App = {
    showers: [],
        toilets: []
  };
