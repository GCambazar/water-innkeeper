import { Action } from '@ngrx/store';

export enum ActionTypes {
    Showers = 'Showers',
    Toilets = 'Toilets'
}

export class Showers implements Action {
    readonly type = ActionTypes.Showers;
    constructor(public payload: {showers: any}) { }
}

export class Toilets implements Action {
    readonly type = ActionTypes.Toilets;
    constructor(public payload: {toilets: any}) { }
}

export type AppAction =
    | Showers
    | Toilets;
