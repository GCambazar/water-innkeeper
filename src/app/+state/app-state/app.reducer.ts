import { AppAction, ActionTypes } from './app.actions';
import { AppState, App } from './app.interfaces';

export function appReducer(state: App, action: AppAction): App {
  switch (action.type) {
    case ActionTypes.Showers: {
      return {
        ...state,
        showers: action.payload.showers
      };
    }
    case ActionTypes.Toilets: {
      return {
        ...state,
        toilets: action.payload.toilets
      };
    }
    default: {
      return state;
    }
  }
}

export const getShowers = (state: AppState) => state.application.showers;
export const getToilets = (state: AppState) => state.application.toilets;
