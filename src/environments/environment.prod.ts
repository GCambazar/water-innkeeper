export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyC3H2Imm1Lhplv1BINTBr6IeMW2168ZH1Y",
    authDomain: "water-innkeeper.firebaseapp.com",
    databaseURL: "https://water-innkeeper.firebaseio.com",
    projectId: "water-innkeeper",
    storageBucket: "water-innkeeper.appspot.com",
  }
};
